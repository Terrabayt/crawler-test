<?php
return [
    'TELEGRAM_CHAT_ID' => env('TELEGRAM_CHAT_ID'),
    'TELEGRAM_BOT_ID' => env('TELEGRAM_BOT_ID')
];
