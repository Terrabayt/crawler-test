After cloning project go to root folder of the project. Then,   
1. clone .env.example file to .env
2. in .env file change TELEGRAM_CHAT_ID to your telegram chat ID
3. run <code>docker-compose up</code>
4. then open [Telegram bot](http://t.me/scrapper_test_bot).
