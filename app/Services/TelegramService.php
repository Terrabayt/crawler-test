<?php


namespace App\Services;


use GuzzleHttp\Client;

class TelegramService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => "https://api.telegram.org/bot" . config('main.TELEGRAM_BOT_ID') . "/sendMessage",
            'allow_redirects' => false
        ]);
    }

    public function send($message){
        $this->client->request('post', '', [
            'query' => [
                'chat_id' => config('main.TELEGRAM_CHAT_ID'),
                'text' => $message,
                'parse_mode' => 'html'
            ]
        ]);
    }
}
