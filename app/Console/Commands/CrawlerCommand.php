<?php

namespace App\Console\Commands;

use App\Services\TelegramService;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class CrawlerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private Client $client;

    private TelegramService $telegramService;

    public function __construct()
    {
        parent::__construct();
        $this->client = new Client(['base_uri' => 'https://aqicn.org/', 'timeout' => 10, 'verify' => false]);
        $this->telegramService = new TelegramService();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = $this->client->get('city/uzbekistan/tashkent/us-embassy/');
        $content = $response->getBody()->getContents();
        $crawler = new Crawler($content);
        $number = $crawler->filter('#aqiwgtvalue')->first()->text();
        $this->telegramService->send("Tashkent US Embassy Air Pollution: <b>$number</b>");
    }
}
